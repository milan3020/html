package com.kaseya;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

import java.io.*;
import java.security.AccessControlException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Selenium {
    private static final String WHERE_TO_SAVE_ROOT = "noImgLogNormalNames/";
    private static final String TYPE = ".html";

    private final String DRIVER_PATH = "chromedriver.exe";
    private final String BASE = "https://sites.google.com/a/spanning.com/spanning-engineering/";
    private static WebDriver webDriver;
    private static JavascriptExecutor js;

    private static final Logger logger = Logger.getLogger(Selenium.class);

    public void setup() {
        System.setProperty("webdriver.chrome.driver", DRIVER_PATH);
        webDriver = new ChromeDriver();
        webDriver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
        webDriver.get(BASE);
        if (webDriver instanceof JavascriptExecutor) {
            js = (JavascriptExecutor) webDriver;
        }
        logger.setLevel(Level.INFO);
    }

    public void parse() {
        List<WebElement> list = webDriver.findElements(By.xpath("html//td[1]//a[@href]"));
        System.out.println(list.size());
        for(int i = 0; i < list.size(); i++) {
            System.out.println(i);
            WebElement webElement = webDriver.findElements(By.xpath("html//td[1]//a[@href]")).get(i);
            String nameFile = createNamesHTML(webElement);
            if(!nameFile.equals(WHERE_TO_SAVE_ROOT + "Engineering Epics" + TYPE)) {
                continue;
            }
            if (i == 224) {continue;} //To change before launch. This file if (name) is dublicated
            System.out.println("FOUND");
            webDriver.navigate().to(webElement.getAttribute("href"));
            String content = "";
            scripts();
            Object result = js.executeScript(
                    "let imageList = [];" +
                            "Array.prototype.slice.call(document.getElementsByTagName('img')).forEach(\n" +
                            "  function(item) {\n" +
                            "   imageList.push(item.src);" +
                            "    item.remove();\n" +
                            "    // or item.parentNode.removeChild(item); for older browsers (Edge-)\n" +
                            "});" +
                            "return JSON.stringify(imageList);" +
                    "");
            logger.info("Problem with file: " + nameFile +"\nIt has " + result + " images. You need to import them seperativetly");
            try {
                content  = webDriver.getPageSource();
            } catch (Exception e) {
                System.out.println(i);
                System.out.println("Mistakes with URLs");
            }
            if (content == null || content.isEmpty()) {
                System.out.println("Mistake with content " + i);
            }
            try {
                createFile(content, nameFile);
            } catch (Exception e) {
                System.out.println("Ошибка создания или записи");
                System.out.println("Название файла: " + nameFile);
                System.out.println("Содержимое файла:" + content);
                e.printStackTrace();
            }
        }
        webDriver.quit();
    }

    public static void createFile(String content, String finalName) throws Exception  {
        File file = new File(finalName);
        if (!file.exists()) {
            file.createNewFile();
        }
        try(BufferedWriter writer = new BufferedWriter(new FileWriter(finalName))) {
            writer.write(content);
        } catch (Exception e) {}

    }

    public static String changeURls(String content) {
        List<WebElement> linksToChange = webDriver.findElements(By.xpath("html//td[1]//a[@href]"));
        for (int i = 0; i < linksToChange.size(); i++) {
            String valueToBeChanged = linksToChange.get(i).getAttribute("href").replaceAll("https://sites.google.com", "");
            String temp = valueToBeChanged.substring(valueToBeChanged.lastIndexOf('/'));
            content = content.replaceAll(valueToBeChanged + "\"", temp + "\"");
        }
        return content;
    }

    private String createNameURL(WebElement webElement) {
        String nameFile;
        nameFile = webElement.getAttribute("href");
        nameFile = nameFile.substring(nameFile.lastIndexOf('/'));
        nameFile = WHERE_TO_SAVE_ROOT + nameFile + TYPE;
        return nameFile;
    }

    private String createNamesHTML(WebElement webElement) {
        String nameFile;
        nameFile = webElement.getAttribute("innerText");
        nameFile = nameFile.replaceAll("[:>/&%|]"," ");
        nameFile = WHERE_TO_SAVE_ROOT + nameFile + TYPE;
        return nameFile;
    }

    private void scripts() {
        js.executeScript("function removeEmojis (string) {\n" +
                "  var regex = /(?:[\\u2700-\\u27bf]|(?:\\ud83c[\\udde6-\\uddff]){2}|[\\ud800-\\udbff][\\udc00-\\udfff]|[\\u0023-\\u0039]\\ufe0f?\\u20e3|\\u3299|\\u3297|\\u303d|\\u3030|\\u24c2|\\ud83c[\\udd70-\\udd71]|\\ud83c[\\udd7e-\\udd7f]|\\ud83c\\udd8e|\\ud83c[\\udd91-\\udd9a]|\\ud83c[\\udde6-\\uddff]|[\\ud83c[\\ude01\\uddff]|\\ud83c[\\ude01-\\ude02]|\\ud83c\\ude1a|\\ud83c\\ude2f|[\\ud83c[\\ude32\\ude02]|\\ud83c\\ude1a|\\ud83c\\ude2f|\\ud83c[\\ude32-\\ude3a]|[\\ud83c[\\ude50\\ude3a]|\\ud83c[\\ude50-\\ude51]|\\u203c|\\u2049|[\\u25aa-\\u25ab]|\\u25b6|\\u25c0|[\\u25fb-\\u25fe]|\\u00a9|\\u00ae|\\u2122|\\u2139|\\ud83c\\udc04|[\\u2600-\\u26FF]|\\u2b05|\\u2b06|\\u2b07|\\u2b1b|\\u2b1c|\\u2b50|\\u2b55|\\u231a|\\u231b|\\u2328|\\u23cf|[\\u23e9-\\u23f3]|[\\u23f8-\\u23fa]|\\ud83c\\udccf|\\u2934|\\u2935|[\\u2190-\\u21ff])/g;\n" +
                "\n" +
                "  return string.replace(regex, '');\n" +
                "}" +
                "document.body.innerHTML = removeEmojis(document.body.innerHTML);");
        js.executeScript("document.getElementById('COMP_042072168038688296').remove();");
        js.executeScript("document.getElementById('sites-page-toolbar').remove();");
        js.executeScript("document.getElementById('sites-chrome-header-wrapper').remove();");
        js.executeScript("document.getElementById('sites-canvas-bottom-panel').remove();");
        js.executeScript("var r = document.getElementsByTagName('script');\n" +
                "\n" +
                "for (var i = (r.length-1); i >= 0; i--) {\n" +
                "\n" +
                "    if(r[i].getAttribute('id') != 'a'){\n" +
                "        r[i].parentNode.removeChild(r[i]);\n" +
                "    }\n" +
                "\n" +
                "}");
        js.executeScript("document.getElementById('sites-chrome-sidebar-left').setAttribute('style', 'display:none');");
    }
}
